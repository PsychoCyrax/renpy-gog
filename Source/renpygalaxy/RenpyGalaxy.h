
#ifndef RENPYGALAXY_H
#define RENPYGALAXY_H

// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the RENPYGALAXY_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// RENPYGALAXY_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

#ifdef RENPYGALAXY_EXPORTS
#define RENPYGALAXY_API __declspec(dllexport)
#else
#define RENPYGALAXY_API __declspec(dllimport)
#endif

#define RENPYGALAXYCPP_CALLTYPE __cdecl

#ifdef __cplusplus
extern "C"
{
#endif

	RENPYGALAXY_API bool RENPYGALAXYCPP_CALLTYPE InitGalaxy(const char* clientID, const char* clientSecret);
	RENPYGALAXY_API bool RENPYGALAXYCPP_CALLTYPE SignInGalaxy();
	RENPYGALAXY_API bool RENPYGALAXYCPP_CALLTYPE CheckSignedIn();
	RENPYGALAXY_API bool RENPYGALAXYCPP_CALLTYPE SignedIn();
	RENPYGALAXY_API bool RENPYGALAXYCPP_CALLTYPE RequestUserStatsAndAchievements();
	RENPYGALAXY_API bool RENPYGALAXYCPP_CALLTYPE SetAchievement(const char* achievement_name);
	RENPYGALAXY_API bool RENPYGALAXYCPP_CALLTYPE StoreStatsAndAchievements();
	RENPYGALAXY_API bool RENPYGALAXYCPP_CALLTYPE CheckGetAchievement(const char* achievement_name);
	RENPYGALAXY_API bool RENPYGALAXYCPP_CALLTYPE GetAchievement(const char* achievement_name);
	RENPYGALAXY_API bool RENPYGALAXYCPP_CALLTYPE ProcessData();

#ifdef __cplusplus
}
#endif


#include <vector>
#include <memory>
#include <galaxy/GalaxyApi.h>

std::vector<std::unique_ptr<galaxy::api::IGalaxyListener>> listeners;

class AuthListener : public galaxy::api::GlobalAuthListener {
public:
	AuthListener();
	virtual void OnAuthSuccess() override;
	virtual void OnAuthFailure(FailureReason reason) override;
	virtual void OnAuthLost() override;
};

class UserStatsAndAchievementsRetrieveListener : public galaxy::api::GlobalUserStatsAndAchievementsRetrieveListener {
public:
	UserStatsAndAchievementsRetrieveListener();
	virtual void OnUserStatsAndAchievementsRetrieveSuccess(galaxy::api::GalaxyID userID) override;
	virtual void OnUserStatsAndAchievementsRetrieveFailure(galaxy::api::GalaxyID userID, FailureReason failureReason) override;
};

class AchievementChangeListener : public galaxy::api::GlobalAchievementChangeListener {
public:
	AchievementChangeListener();
	virtual void OnAchievementUnlocked(const char* name) override;
};

class StatsAndAchievementsStoreListener : public galaxy::api::GlobalStatsAndAchievementsStoreListener {
public:
	StatsAndAchievementsStoreListener();
	virtual void OnUserStatsAndAchievementsStoreSuccess() override;
	virtual void OnUserStatsAndAchievementsStoreFailure(FailureReason failureReason) override;
};


#endif
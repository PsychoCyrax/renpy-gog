**Renpy-GOG Achievement Library**

This is a small ctypes library and a python helper class to have simple achievement flags synced to GOG in your Renpy game.

To use this library:
 1. Download the GOG Galaxy SDK for your target platform (Windows 32bit/64bit and/or OSX) from the GOG GALAXY Developer Portal: https://devportal.gog.com/galaxy/components/sdk.
 2. Copy the downloaded library file from the "Libraries" folder to the respective "renpy-gog/lib/{platform}" folders.
 3. Copy the contents of "renpy-gog" folder to the root folder of your Renpy game, beside the "game" folder.

See "example.rpy" for usage example.

To build the library on your own for Windows with Visual Studio, download the GOG Galaxy SDK for Windows 32bit and 64bit from the GOG GALAXY Developer Portal: https://devportal.gog.com/galaxy/components/sdk. Extract the files and copy them into "Source/galaxy-sdk/Windows_MSVC19_32bit" and "Source/galaxy-sdk/Windows_MSVC19_64bit" folder respectively.
